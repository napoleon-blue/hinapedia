Input
  = _ p:Pairs {return p}

Pairs
  = i1:Pair "," _ i2:Pairs  { return [i1].concat(i2) }
  / i:Pair {return [i]}

Pair
  = v:Value _ o:Order _ {
  	if (v.eval) {
  		return {eval: v.eval, asc: o};
    } else {
	  	return {value: v.value, asc: o};
    }
  }

Value
  = '"' chars:DoubleStringCharacter* '"e' { return {eval: chars.join('')} }
  / chars:[^ \t\n\r,]* { return {value: chars.join('')} }

DoubleStringCharacter
  = !('"' / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

EscapeSequence
  = "'"
  / '"'
  / "\\"
  / "b"  { return "\b";   }
  / "f"  { return "\f";   }
  / "n"  { return "\n";   }
  / "r"  { return "\r";   }
  / "t"  { return "\t";   }
  / "v"  { return "\x0B"; }

Order
  = "ASC"i { return true }
  / "DESC"i { return false }
  / "" { return true }

_ "whitespace"
  = [ \t\n\r]*