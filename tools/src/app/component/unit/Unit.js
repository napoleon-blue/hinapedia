/**
 * Created by tottokotkd on 29/01/2017.
 */

import React, {Component, PropTypes} from 'react'
import {intlShape} from 'react-intl';
import {Link} from 'react-router'
import {Header, Table, Grid, List, Message} from 'semantic-ui-react'
import defaultTo from 'lodash/defaultTo'

import {UNIT_SEARCH_STATUS} from '../../constant/Mode'

class Unit extends Component {

    render() {
        const {status} = this.props;

        switch (status) {
            case UNIT_SEARCH_STATUS.NOT_FOUND: {
                const {name} = this.props.params;
                return <div>{name} not found</div>;
            }

            case UNIT_SEARCH_STATUS.FOUND: {
                const {unitID, unitName, idols} = this.props.unit;
                const header = `${unitName}`;
                const googleURL = `https://www.google.com/search?q=シンデレラガールズ+${unitName}`;
                const tagURL = `http://www.pixiv.net/tags.php?tag=${unitName}`;
                const dicURL = `http://dic.pixiv.net/a/${unitName}`;

                return (
                    <Grid>
                        <Grid.Row>
                            <Grid.Column>
                                <Message info icon='bomb'
                                         header='データ収集にご協力ください'
                                         content={<div>ユニットデータは現在整備中です。追加・修正リクエストはこちら (<a href="https://twitter.com/tottokotkd">Twitter</a> / <a href="https://gitlab.com/tottokotkd/hinapedia/issues/9">GitLab</a>)</div>}
                                />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column width={10}>
                                <Header as="h3" content={header}/>
                                <Header as='h4' content='Google'/>
                                <List link>
                                    <List.Item as='a' icon="linkify" href={googleURL} content="search"/>
                                </List>
                                <Header as='h4' content='Pixiv'/>
                                <List link>
                                    <List.Item as='a' icon="linkify" href={tagURL} content="tag"/>
                                    <List.Item as='a' icon="linkify" href={dicURL} content="dic"/>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={6}>

                                <Header as='h4' attached='top' content='所属アイドル'/>
                                <Table attached='bottom'>
                                    <Table.Body>
                                        {
                                            defaultTo(idols, []).map(idol =>
                                                <Table.Row key={idol.characterID}>
                                                    <Table.Cell selectable textAlign="left">
                                                        <Link to={`/idol/${idol.characterName}`}>{idol.characterName}</Link>
                                                    </Table.Cell>
                                                </Table.Row>
                                            )
                                        }
                                    </Table.Body>
                                </Table>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                );
            }

            default:
                return null;
        }
    }
}

Unit.propTypes = {
    status: PropTypes.symbol.isRequired,
    unit: PropTypes.shape({
        unitID: PropTypes.number.isRequired,
        unitName: PropTypes.string.isRequired,
        idols: PropTypes.arrayOf(PropTypes.shape({
            characterID: PropTypes.number.isRequired,
            characterName: PropTypes.string.isRequired,
        })),
    }),

    // react-router
    params: PropTypes.shape({
        name: PropTypes.string.isRequired,
    }).isRequired,

    // react-intl
    intl: intlShape
};

export default Unit;
