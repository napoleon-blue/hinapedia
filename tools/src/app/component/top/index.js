/**
 * Created by tottokotkd on 31/01/2017.
 */


import React from 'react'
const {Component, PropTypes} = React;

import {intlShape} from 'react-intl';
import {Link} from 'react-router'
import {Container, Message, Header, Divider, List} from 'semantic-ui-react'

class TopIndex extends Component {
    render() {
        return (
            <Container text>
                <Message
                    info
                    icon='hand paper'
                    header='このプロジェクトは開発中です'
                    content='中の人の気分次第ですごい変更が入ったりします'
                />

                <Header  as='h3' content='これはなに'/>
                <div>　シンデレラガールズの設定を収集するプロジェクトです。2017年2月現在<a href="https://gitlab.com/tottokotkd/hinapedia/boards">とても開発中</a>です。</div>

                <Header  as='h3' content='くわしく'/>
                <List bulleted>
                    <List.Item><a href="/hinapedia/info">開発ブログ</a>
                        <List.List>
                            <List.Item><a href="/hinapedia/info/permalink/ciyu4m5l80005agvk6biwigc5/">このプロジェクトについて</a></List.Item>
                        </List.List>
                    </List.Item>
                    <List.Item><a href="https://gitlab.com/tottokotkd/hinapedia/">ソースコード</a>
                        <List.List>
                            <List.Item><a href="https://gitlab.com/tottokotkd/hinapedia/issues/9">ユニットデータ追加・修正</a></List.Item>
                        </List.List>
                    </List.Item>
                </List>

            </Container>
        );
    }
}

export default TopIndex
