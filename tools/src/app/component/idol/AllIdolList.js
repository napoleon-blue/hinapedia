/**
 * Created by tottokotkd on 09/02/2017.
 */

import React, {Component, PropTypes} from 'react'
import {routerShape, locationShape, Link} from 'react-router'
import {intlShape} from 'react-intl';
import {
    Input,
    Header,
    Segment,
    Button,
    Grid, GridRow, GridColumn,
    Message, MessageHeader,
    List, ListItem,
} from 'semantic-ui-react'
import {Style} from 'radium'
import trim from 'lodash/trim'
import filter from 'lodash/filter'
import flow from 'lodash/flow'
import replace from 'lodash/replace'

import IdolDisplayTable from './IdolDisplayTable'
import {headerMessages} from '../../intl/messages'

const text = content => <p style={{textIndent:"1em"}}>{content}</p>

class SearchQueryInput extends Component {

    state = {
        show: false
    };

    render() {
        const {query, set} = this.props;
        const {show} = this.state;
        const styleName = 'temp-message-style1';
        const sampleNode = q => <p onClick={() => set(q)}>{q}</p>
        return (
            <Grid>
                <GridRow>
                    <GridColumn>
                        <Input fluid
                               icon={{
                                   name: 'question', circular: true, link: true,
                                   inverted: show,
                                   onClick: () => this.setState({show: !show})
                               }}
                               placeholder='filter'
                               value={query}
                               onChange={(event, data) => set(data.value)}
                        />
                    </GridColumn>
                </GridRow>
                {show ?
                    <GridRow>
                        <GridColumn>
                            <Style
                                scopeSelector={`.${styleName}`}
                                rules={{
                                    p: {
                                        textIndent:"1em"
                                    }
                                }}
                            />
                            <Message info className={styleName}>

                                <Segment basic>
                                    <Header as="h3" content="フィルタークエリーについて" dividing/>
                                    <p>Apache Lucene風のフィルタークエリーを記述することで、表示データを絞り込むことができます。以下のサンプルコードをクリックすると、実際にフィルタークエリーを利用することができます。</p>
                                    <p>フィルター後のデータを並べ替えるには、ソートクエリーを利用するか、表のヘッダーをクリックしてください。</p>
                                </Segment>

                                <Segment basic>

                                    <Header as="h4" content="基本構文" />

                                    <p>フィルター対象を左に、フィルター条件を右に記述します。その間に半角コロンを挟みます。</p>
                                    <Message>
                                        {sampleNode('age:17')}
                                    </Message>

                                    <p>左右の文字列とコロンの間にスペースを開けたり、複数のコロンを含めることはできません。左右の値がスペースや半角コロンを含む場合は、その値全体を半角引用符で挟みます。</p>
                                    <Message>
                                        {sampleNode('unit:"LOVE LAIKA"')}
                                        {sampleNode('unit:"Masque:Rade"')}
                                    </Message>

                                    <p>フィルター対象を省略すると、デフォルトの検索対象が設定されます。現在のデフォルト検索対象は「アイドルの名前・読み」です。</p>
                                    <Message>
                                        {sampleNode('あい')}
                                    </Message>

                                    <p>上記クエリーは「名前に "あい" を含むか、名前の読みに "あい" を含むアイドル」を意味します。</p>
                                </Segment>

                                <Segment basic>
                                    <Header as="h4" content="論理演算子" />

                                    <p>有効な検索条件であれば、論理演算子によって結合することができます。使用できる演算子は AND, OR, NOT の3つです。これらの演算子は半角大文字でなければいけません。</p>
                                    <Message>
                                        {sampleNode('unit:ブルーナポレオン AND age:20')}
                                        {sampleNode('unit:うみのようせい OR unit:ミナヅキゴコロ')}
                                        {sampleNode('unit:蒼ノ楽団 NOT unit:シンデレラプロジェクト')}
                                    </Message>

                                    <p>論理演算子は括弧を併用することができます。</p>
                                    <Message>
                                        {sampleNode('(unit:うみのようせい OR unit:ミナヅキゴコロ) AND type:pa')}
                                        {sampleNode('(voice:true AND type:co AND unit:プロジェクトクローネ) OR (unit:L.M.B.G AND age:12)')}
                                    </Message>

                                    <p>なお、論理演算子を1つの検索条件に対して利用することはできません。したがって「シンデレラプロジェクトに所属しないアイドル」を以下のように表現することはできません。</p>
                                    <Message>
                                        {sampleNode('NOT unit:シンデレラプロジェクト')}
                                    </Message>

                                    <p>このようなクエリーを記述する場合は、マイナス符号を用いてください。</p>
                                    <Message>
                                        {sampleNode('unit:-シンデレラプロジェクト')}
                                    </Message>
                                </Segment>

                                <Segment basic>
                                    <Header as="h4" content="範囲演算子" />

                                    <p>数値をあらわす項目に対しては、範囲演算子を利用することができます。範囲演算子を利用するには、左側に始点、右側に終点を記述し、両者を ".." で接続します。</p>
                                    <Message>
                                        {sampleNode('height:120..140')}
                                    </Message>

                                    <p>範囲演算子は始点のみ・終点のみを指定することも可能です。</p>
                                    <Message>
                                        {sampleNode('unitCount:17..')}
                                        {sampleNode('age:..17')}
                                    </Message>
                                </Segment>

                                <Segment basic>
                                    <Header as="h4" content="フィルター対象リスト" />

                                    <p>アイドル一覧ページで利用できるフィルター対象は以下の通りです。</p>

                                    <List divided>
                                        <ListItem
                                            header="文字列型 (部分一致)"
                                            content="name, yomi, actor"
                                        />
                                        <ListItem
                                            header="文字列型 (完全一致)"
                                            content="NAME, YOMI, ACTOR, type, blood, unit"
                                        />
                                        <ListItem
                                            header="数値型"
                                            content="height, weight, age, bust, waist, hip, unitCount, birthday (数字のみ4桁表記)"
                                        />
                                        <ListItem
                                            header="真偽型"
                                            content="voice (true, yes, t, y を入力すると声あり、それ以外は声なし)"
                                        />
                                    </List>

                                </Segment>
                            </Message>
                        </GridColumn>
                    </GridRow>
                    : null}
            </Grid>
        );
    }
}

class SortQueryInput extends Component {

    state = {
        show: false
    };

    render() {
        const {query, set} = this.props;
        const {show} = this.state;
        const styleName = 'temp-message-style2';
        const sampleNode = q => <p onClick={() => set(q)}>{q}</p>
        return (
            <Grid>
                <GridRow>
                    <GridColumn>
                        <Input fluid
                               icon={{
                                   name: 'question', circular: true, link: true,
                                   inverted: show,
                                   onClick: () => this.setState({show: !show})
                               }}
                               placeholder='sort'
                               value={query}
                               onChange={(event, data) => set(data.value)}
                        />
                    </GridColumn>
                </GridRow>
                {show ?
                    <GridRow>
                        <GridColumn>

                            <Style
                                scopeSelector={`.${styleName}`}
                                rules={{
                                    p: {
                                        textIndent:"1em"
                                    }
                                }}
                            />
                            <Message info className={styleName}>

                                <Segment basic>
                                    <Header as="h3" content="ソートクエリーについて" dividing/>
                                    <p>SQL風のソートクエリーを記述することで、表示データを並べ替えることができます。以下のサンプルコードをクリックすると、実際にソートクエリーを利用することができます。</p>
                                    <p>ソート対象のデータを絞り込むには、フィルタークエリーを利用してください。</p>
                                </Segment>

                                <Segment basic>

                                    <Header as="h4" content="構文" />

                                    <p>ソートの基準にしたい対象データをカンマ区切りで列挙します。</p>
                                    <Message>
                                        {sampleNode('type, age, name')}
                                    </Message>

                                    <p>列挙した対象は左から順に評価され、それぞれ昇順でソートされます。</p>

                                    <p>明示的に昇順 (asc) または降順 (desc) を指定することもできます。たとえば以下のクエリーは「年齢の小さい方からソートし、同年齢であるときは身長の大きい方からソートする」ことを意味します。</p>
                                    <Message>
                                        {sampleNode('age asc, height desc')}
                                    </Message>

                                </Segment>

                                <Segment basic>
                                    <Header as="h4" content="ソート対象リスト" />

                                    <p>アイドル一覧ページで利用できるソート対象は以下の通りです。</p>

                                    <List divided>
                                        <ListItem
                                            content="name, yomi, type, height, weight, age, bust, waist, hip, unitCount, birthday"
                                        />
                                    </List>

                                </Segment>
                            </Message>
                        </GridColumn>
                    </GridRow>
                    : null}
            </Grid>
        );
    }
}

class AllIdolList extends Component {

    render() {
        const {list, searchQuery, setSearchQuery, sortQuery, setSortQuery, router} = this.props;
        const {formatMessage} = this.props.intl;
        const idols = filter(list, 'isMatch');

        const push = (f, s) => {
            const convert = flow(
                trim,
                str => replace(str, /\s+/g, " "),
                encodeURIComponent,
            );
            const filter = convert(f);
            const sort = convert(s);
            const {pathname} = this.props.location;
            if (filter && sort) {
                router.push({pathname, query: {filter, sort}});
            } else if (filter) {
                router.push({pathname, query: {filter}});
            } else if (sort) {
                router.push({pathname, query: {sort}});
            } else {
                router.push(pathname);
            }
        };

        const search = q => {
            setSearchQuery(q);
            push(q, sortQuery);
        };

        const sort = q => {
            setSortQuery(q);
            push(searchQuery, q);
        };

        return (
            <div>
                <Grid container verticalAlign='middle'>
                    <GridRow>
                        <GridColumn>
                            <SearchQueryInput query={searchQuery} set={search}/>
                        </GridColumn>
                    </GridRow>
                    <Grid.Row>
                        <GridColumn>
                            <SortQueryInput query={sortQuery} set={sort}/>
                        </GridColumn>
                    </Grid.Row>
                    <GridRow>
                        <GridColumn>
                            <IdolDisplayTable idols={idols} showAverage/>
                        </GridColumn>
                    </GridRow>
                    <GridRow>
                        <GridColumn textAlign="right" verticalAlign="middle">
                            <Button as='a' circular color='twitter' icon='twitter'
                                    href={`https://twitter.com/intent/tweet?text=${"hinapedia: シンデレラガールズ一覧"}&url=${encodeURIComponent(window.location.href)}&hashtags=hinapedia`}
                            />
                        </GridColumn>
                    </GridRow>
                </Grid>
            </div>
        )
    }
}

AllIdolList.propTypes = {
    searchQuery: PropTypes.string.isRequired,
    sortQuery: PropTypes.string.isRequired,

    mode: PropTypes.symbol.isRequired,
    list: IdolDisplayTable.propTypes.idols,
    setSearchQuery: PropTypes.func.isRequired,
    setSortQuery: PropTypes.func.isRequired,
    reload: PropTypes.func.isRequired,

    // react-intl
    intl: intlShape,

    // react-router
    children: PropTypes.node,
    router: routerShape.isRequired,
    location: locationShape.isRequired,
};

AllIdolList.defaultProps = {
};

export default AllIdolList;
