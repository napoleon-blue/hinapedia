/**
 * Created by tottokotkd on 2017/01/11.
 */

import * as Type from '../constant/ActionType'

export const selectMenuItem = menuItem => {
    return {type: Type.SELECT_MENU_ITEM, menuItem};
};
