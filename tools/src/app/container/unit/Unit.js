/**
 * Created by tottokotkd on 30/01/2017.
 */

import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {injectIntl} from 'react-intl'

import Component from '../../component/unit/Unit'
import * as Selector from '../../selector'
import {searchUnit} from '../../action/unit'

const mapStateToProps = state => {
    const {unit, status} = Selector.getSelectedUnit(state);
    return {unit, status}
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Component));
