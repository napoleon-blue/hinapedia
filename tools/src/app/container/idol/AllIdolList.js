import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {injectIntl} from 'react-intl'

import Component from '../../component/idol/AllIdolList'
import * as Selector from '../../selector'
import * as Action from '../../action/idol'

const mapStateToProps = state => {
    return {
        searchQuery: Selector.getIdolSearchQuery(state),
        sortQuery: Selector.getIdolSortQuery(state),
        mode: Selector.getSearchMode(state),
        list : Selector.getAllIdolsList(state),
    }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        reload: Action.loadAllIdols,
        setSearchQuery: Action.setIdolListSearchQuery,
        setSortQuery: Action.setIdolListSortQuery,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Component));
