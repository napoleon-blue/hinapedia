/**
 * Created by tottokotkd on 30/01/2017.
 */

import * as React from 'react'
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {injectIntl} from 'react-intl'

import Idol from '../../component/idol/Idol'
import * as Selector from '../../selector'
import * as Action from '../../action/idol'

const mapStateToProps = state => {
    const {idol, status} = Selector.getSelectedIdol(state);
    return {idol, status}
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(Idol));
