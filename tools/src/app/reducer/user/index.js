/**
 * Created by tottokotkd on 25/01/2017.
 */

import * as Type from '../../constant/ActionType'
import * as Mode from '../../constant/Mode'
import assign from 'lodash/assign'

import {languages, msgData} from '../../intl'

const defaultState = () => {
    return {
        lang: msgData[languages.ja.code]
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);
    switch (action.type) {
        default:
            return state;
    }
};
