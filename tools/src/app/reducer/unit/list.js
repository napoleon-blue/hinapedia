/**
 * Created by tottokotkd on 02/02/2017.
 */

import assign from 'lodash/assign'
import filter from 'lodash/filter'
import some from 'lodash/some'
import every from 'lodash/every'

import * as Type from '../../constant/ActionType'
import {UNIT_SEARCH_MODE} from '../../constant/Mode'

const defaultState = () => {
    return {
        loading: true,
        unitSearchQuery: '',
        searchMode: UNIT_SEARCH_MODE.AND,
        allUnits: [],
        page: 1,
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);

    const search = (units, query, mode) => {

        if (!query) return units;

        const queries = query.split(/\s/).map(q => q.trim().toLowerCase()).filter(q => !!q);

        const include = (unit, query) => unit.unitName.toLowerCase().includes(query)
        || some(unit.idols, idol => idol.characterName.includes(query) || idol.yomi.replace(',', '').includes(query) || idol.idolType.toLowerCase() == query);

        const mapper = (() => {
            if (mode == UNIT_SEARCH_MODE.AND) {
                return every;
            }
            if (mode == UNIT_SEARCH_MODE.OR) {
                return some;
            }
        })();

        return units.map(unit => {
            const isMatch = mapper(queries, q => include(unit, q));
            return {...unit, isMatch};
        });
    };

    switch (action.type) {

        case Type.SET_UNIT_LIST_SEARCH_MODE: {
            return update({
                searchMode: action.mode,
                allUnits: search(state.allUnits, state.unitSearchQuery, action.mode)
            });
        }

        case Type.SET_UNIT_LIST_SEARCH_QUERY: {
            return update({
                unitSearchQuery: action.query,
                allUnits: search(state.allUnits, action.query, state.searchMode)
            });
        }

        case Type.SET_UNIT_LIST_PAGE: {
            return update({page: action.page});
        }

        case Type.LOAD_UNITS_DATA: {
            const {data} = action;
            const allUnits = data.map(d => {
                const actorCount = filter(d.idols, idol => idol.actorName).length;
                return {actorCount, ...d}
            });
            return update({allUnits, loading: false,});
        }
    }

    return state;
};
