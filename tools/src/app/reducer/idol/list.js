/**
 * Created by tottokotkd on 01/02/2017.
 */

import includes from 'lodash/includes'
import orderBy from 'lodash/orderBy'
import assign from 'lodash/assign'
import every from 'lodash/every'
import some from 'lodash/some'
import find from 'lodash/find'
import flow from 'lodash/flow'

import * as Type from '../../constant/ActionType'
import QueryParser from '../../lib/QueryParser'
import SortOrderParser from '../../lib/sort-order'
import {IDOL_SEARCH_MODE, IDOL_SEARCH_STATUS} from '../../constant/Mode'

const defaultState = () => {
    return {
        loading: true,
        idolSearchQuery: '',
        idolSortQuery: '',
        searchMode: IDOL_SEARCH_MODE.OR,
        allIdols: [],
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);

    const search = (idols, query) => {

        if (!query) {
            return idols.map(idol => {return {...idol, isMatch: true};});
        }

        const checker = new QueryParser(query => {
            const {field, term} = query;

            switch (field) {
                case 'name':
                    return target => {
                        if (field === 'NAME') {
                            return target.characterName === term;
                        } else {
                            return target.characterName.includes(term);
                        }
                    };

                case 'yomi':
                    return target => {
                        const yomi = target.yomi.replace(',');
                        if (field === 'YOMI') {
                            return yomi === term;
                        } else {
                            return yomi.includes(term);
                        }
                    };

                case 'actor':
                    return target => {
                        if (!target.actor) {
                            return false;
                        }

                        if (field === 'ACTOR') {
                            return target.actor === term;
                        } else {
                            return target.actor.includes(term);
                        }
                    };

                case 'birthday': {
                    const {month, day} = target.birthDate;
                    return target => QueryParser.rangeValueChecker(query)(month * 100 + day);
                }

                case 'type':
                    return target => target.idolType.toLowerCase() === term.toLowerCase();

                case 'blood':
                    return target => target.bloodType.toLowerCase() === term.toLowerCase();

                case 'unit':
                    return target => includes(target.units, term);

                case 'unitCount':
                    return target => QueryParser.rangeValueChecker(query)(target.units.length);

                case 'colleague':
                    return target => {
                        const idol = find(idols, ['characterName', term]);
                        return idol == null || idol.units == null
                            ? false
                            :　target.characterName != term && some(target.units, unit => includes(idol.units, unit));
                    };

                case 'voice':
                    return target => {
                        switch (term.toLowerCase()) {
                            case 't':
                            case 'true':
                            case 'y':
                            case 'yes':
                                return !!target.actor;

                            default:
                                return !target.actor;
                        }
                    };

                case 'height':
                case 'weight':
                case 'age':
                case 'bust':
                case 'waist':
                case 'hip':
                case 'colleagueCount':
                case 'cvColleagueCount':
                case 'cvColleagueRate':
                    return target => QueryParser.rangeValueChecker(query)(target[field]);

                default:
                    return target => {
                        const yomi = target.yomi.replace(',');
                        return target.characterName.includes(term) || yomi.includes(term) || target.idolType === term;
                    };
            }

        }).parseSafely(query);

        return idols.map(idol => {
            const isMatch = checker(idol);
            return {...idol, isMatch};
        });
    };

    const sort = (idols, query) => {

        if (!query) {
            return idols;
        }

        const data = SortOrderParser.parse(query);
        const mapper = d => {
            if (d.eval) {
                return new Function('{characterName}', `return ${d.eval}`);

            } else {
                switch (d.value) {
                    case 'name':
                    case 'yomi':
                        return 'yomi';

                    case 'type':
                        return 'idolType';

                    case 'birthday':
                        return idol => idol.birthDate.month * 100 +idol.birthDate.day;

                    case 'age':
                    case 'height':
                    case 'weight':
                    case 'waist':
                    case 'bust':
                    case 'hip':
                    case 'colleagueCount':
                    case 'cvColleagueCount':
                    case 'cvColleagueRate':
                        return d.value;

                    case 'units':
                    case 'unitCount':
                        return idol => idol.unitCount;

                    default:
                        return null;
                }
            }
        };
        const request = data.map(mapper);
        return every(request, r => r != null)
            ? orderBy(idols, data.map(mapper), data.map(d => d.asc ? 'asc': 'desc'))
            : idols;
    };

    switch (action.type) {

        case Type.SET_IDOL_LIST_SEARCH_QUERY: {
            return update({
                idolSearchQuery: action.query,
                allIdols: search(state.allIdols, action.query)
            });
        }

        case Type.SET_IDOL_LIST_SORT_QUERY: {
            return update({
                idolSortQuery: action.query,
                allIdols: sort(state.allIdols, action.query)
            });
        }

        case Type.LOAD_IDOLS_DATA: {
            const allIdols = flow(
                idols => idols.map(d => {return {...d, isMatch: true}}),
                idols => search(idols, state.idolSearchQuery),
                idols => sort(idols, state.idolSortQuery),
            )(action.data);
            return update({allIdols, loading: false,});
        }
    }

    return state;
};
