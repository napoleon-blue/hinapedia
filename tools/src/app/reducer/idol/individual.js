/**
 * Created by tottokotkd on 01/02/2017.
 */

import assign from 'lodash/assign'
import filter from 'lodash/filter'

import * as Type from '../../constant/ActionType'
import {IDOL_SEARCH_MODE, IDOL_SEARCH_STATUS} from '../../constant/Mode'

const defaultState = () => {
    return {
        status: IDOL_SEARCH_STATUS.EMPTY,
        query: null,
        idolData: null,
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);

    switch (action.type) {
        case Type.SET_IDOL_SEARCH_QUERY: {
            return update({query: action.query});
        }

        case Type.SET_IDOL_SEARCH_STATUS: {
            return update({status: action.status});
        }

        case Type.LOAD_IDOL_INDIVIDUAL_DATA: {

            const {data} = action;
            const units = data.units.map(u => {
                const actorCount = filter(u.idols, idol => idol.actorName).length;
                return {actorCount, ...u}
            });

            const idolData = {...data, units};
            return update({idolData});
        }
    }

    return state;
};
