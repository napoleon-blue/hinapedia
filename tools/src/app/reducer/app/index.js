/**
 * Created by tottokotkd on 2017/01/11.
 */

import assign from 'lodash/assign'

import * as Type from '../../constant/ActionType'
import {MENU_ITEM} from '../../constant/Mode'

const defaultState = () => {
    return {
        selectedMenuItem: MENU_ITEM.IDOL
    }
};

export default (state = defaultState(), action) => {
    const update = newValue => assign({}, state, newValue);

    switch (action.type) {
        case Type.SELECT_MENU_ITEM:
            return update({selectedMenuItem: action.menuItem})
    }

    return state;
};
