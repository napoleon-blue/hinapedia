/**
 * Created by tottokotkd on 03/01/2017.
 */

import {createSelector} from'reselect'
import sortBy from 'lodash/sortBy'

import {IDOL_SEARCH_STATUS, UNIT_SEARCH_STATUS} from '../constant/Mode'

/*
    app
*/
export const getSelectedMenuItem = createSelector(
    state => state.app.selectedMenuItem,
    selectedMenuItem => selectedMenuItem
);

/*
 idol.list
 */
export const getIsIdolListLoading = createSelector(
    state => state.idol.list.loading,
    loading => loading
);

export const getIdolSearchQuery = createSelector(
    state => state.idol.list.idolSearchQuery,
    query => query
);

export const getIdolSortQuery = createSelector(
    state => state.idol.list.idolSortQuery,
    query => query
);

export const getSearchMode = createSelector(
    state => state.idol.list.searchMode,
    mode => mode
);

export const getAllIdolsList = createSelector(
    state => state.idol.list.allIdols,
    allIdols => allIdols
);

/*
 idol
 */
export const getSelectedIdol = createSelector(
    state => state.idol.individual.status,
    state => state.idol.individual.idolData,
    (status, idolData) => {return {status, idol: idolData}}
);

export const getIdolIndividualDataIsLoading = createSelector(
    state => state.idol.individual.status,
    status => status == IDOL_SEARCH_STATUS.LOADING
);


/*
 unit.list
 */
export const getIsUnitListLoading = createSelector(
    state => state.unit.list.loading,
    loading => loading
);

export const getUnitSearchQuery = createSelector(
    state => state.unit.list.unitSearchQuery,
    unitSearchQuery => unitSearchQuery
);

export const getUnitSearchMode = createSelector(
    state => state.unit.list.searchMode,
    mode => mode
);


export const getUnitListPage = createSelector(
    state => state.unit.list.page,
    page => page
);

export const getAllUnitsList = createSelector(
    state => state.unit.list.allUnits,
    allIdols => allIdols
);

/*
 unit
 */
export const getSelectedUnit = createSelector(
    state => state.unit.individual.status,
    state => state.unit.individual.unitData,
    (status, unitData) => {return {status, unit: unitData}}
);

export const getUnitIndividualDataIsLoading = createSelector(
    state => state.unit.individual.status,
    status => status == UNIT_SEARCH_STATUS.LOADING
);


/*
    user conf
 */
export const getLanguages = createSelector(
    state => state.user.lang.languages,
    languages => {
        const keys = Object.values(languages);
        return sortBy(keys, 'code')
    }
);

export const watchLanguage = createSelector(
    state => state.user.lang.locale,
    locale => locale
);
