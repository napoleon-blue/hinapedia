#!/usr/bin/env bash

#docker-compose up -d
docker-compose exec db psql -f /hinapedia/sql/02_import.sql -U postgres -d hinaDB -v path=/hinapedia/data
docker-compose exec db psql -f /hinapedia/sql/03_views.sql -U postgres -d hinaDB
docker-compose exec db psql -f /hinapedia/sql/output.sql -U postgres -d hinaDB -v path=/hinapedia/dist
