DROP SCHEMA IF EXISTS hinapedia CASCADE;
CREATE SCHEMA hinapedia;
SET SEARCH_PATH TO hinapedia;

CREATE EXTENSION btree_gist;
CREATE COLLATION "ja_JP.utf8" (LOCALE = 'ja_JP.UTF-8');

/*
  character
*/

CREATE TABLE characters (
    id INT PRIMARY KEY,
    name TEXT COLLATE "ja_JP.utf8" NOT NULL
);

CREATE TABLE yomi_of_character (
    character_id INT PRIMARY KEY ,
    yomi TEXT COLLATE "ja_JP.utf8" NOT NULL ,
    FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE birthday_of_character (
    character_id INT PRIMARY KEY ,
    birth_month INT,
    birth_day INT,
    FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE age_of_character (
  character_id INT PRIMARY KEY ,
  age INT NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE height_of_character (
  character_id INT PRIMARY KEY ,
  metre DECIMAL(4, 3) NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE weight_of_character (
  character_id INT PRIMARY KEY ,
  kilogramme DECIMAL(5, 3) NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE bwh_measurement_of_character (
  character_id INT PRIMARY KEY ,
  bust_metre DECIMAL(4, 3) NOT NULL ,
  waist_metre DECIMAL(4, 3) NOT NULL ,
  hip_metre DECIMAL(4, 3) NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id)
);

CREATE TABLE handedness (
  value TEXT PRIMARY KEY
);

CREATE TABLE handedness_of_character (
  character_id INT PRIMARY KEY ,
  handedness TEXT NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id),
  FOREIGN KEY (handedness) REFERENCES handedness (value)
);

CREATE TABLE blood_types (
  value TEXT PRIMARY KEY
);

CREATE TABLE blood_type_of_character (
  character_id INT PRIMARY KEY ,
  blood_type TEXT NOT NULL ,
  FOREIGN KEY (character_id) REFERENCES characters (id),
  FOREIGN KEY (blood_type) REFERENCES blood_types (value)
);

/*
  actor
*/
CREATE TABLE actors (
  id INT PRIMARY KEY ,
  name TEXT COLLATE "ja_JP.utf8" NOT NULL
);

CREATE TABLE actor_of_character (
  character_id INT NOT NULL ,
  actor_id INT NOT NULL ,
  PRIMARY KEY (character_id, actor_id),
  FOREIGN KEY (character_id) REFERENCES characters (id),
  FOREIGN KEY (actor_id) REFERENCES actors (id)
);

/*
  idol
 */

CREATE TABLE idol_types (
    value TEXT PRIMARY KEY
);

CREATE TABLE idol_type_of_character (
    character_id INT PRIMARY KEY ,
    idol_type TEXT NOT NULL ,
    FOREIGN KEY (character_id) REFERENCES characters (id),
    FOREIGN KEY (idol_type) REFERENCES idol_types (value)
);

/*
  unit
*/

CREATE TABLE units (
  id INT PRIMARY KEY ,
  name TEXT COLLATE "ja_JP.utf8" NOT NULL UNIQUE
);

CREATE TABLE characters_of_unit (
  unit_id INT NOT NULL ,
  character_id INT NOT NULL ,
  range DATERANGE NOT NULL DEFAULT DATERANGE(null, null),
  PRIMARY KEY (unit_id, character_id, range),
  FOREIGN KEY (character_id) REFERENCES characters (id),
  FOREIGN KEY (unit_id) REFERENCES units (id),
  EXCLUDE USING GIST (character_id WITH =, unit_id WITH =, range with &&)
);

/*
  song
*/

CREATE TABLE songs (
  id INT PRIMARY KEY ,
  name TEXT COLLATE "ja_JP.utf8" NOT NULL
);

CREATE TABLE records (
  id INT PRIMARY KEY,
  song_id INT NOT NULL ,
  FOREIGN KEY (song_id) REFERENCES songs (id)
);

CREATE TABLE singer_of_records (
  record_id INT NOT NULL ,
  character_id INT NOT NULL ,
  actor_id INT NOT NULL ,
  PRIMARY KEY (record_id, character_id, actor_id),
  FOREIGN KEY (record_id) REFERENCES records (id),
  FOREIGN KEY (character_id) REFERENCES characters (id),
  FOREIGN KEY (actor_id) REFERENCES actors (id)
);

/*
  album
*/

CREATE TABLE albums (
  id INT PRIMARY KEY ,
  name TEXT COLLATE "ja_JP.utf8" NOT NULL
);

CREATE TABLE records_of_album (
  album_id INT NOT NULL ,
  record_id INT NOT NULL ,
  track INT,
  PRIMARY KEY (album_id, record_id),
  FOREIGN KEY (album_id) REFERENCES albums (id),
  FOREIGN KEY (record_id) REFERENCES records (id)
);
